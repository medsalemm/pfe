<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8" />

        <meta name="application-name" content="{{ config('app.name') }}" />
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />

        <title>{{ $title ?? config('app.name') }}</title>

        <style>
            [x-cloak] {
                display: none !important;
            }
        </style>

        @filamentStyles
        @vite(['resources/css/app.css','resources/js/app.js'])
        @livewireStyles

    </head>

    <body class="antialiased bg-slate-200 dark:bg-slate-700">
        @livewire('partials.navbar')
    <main>
        {{ $slot }}

    </main>
    @livewire('partials.footer')
        @filamentScripts
        @livewireScripts
        <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
        <x-livewire-alert::scripts />
        @vite('resources/js/app.js')
    </body>
</html>
