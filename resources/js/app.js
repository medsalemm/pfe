import 'preline'
document.addEventListener('livewire:navigated', () => { 
    if (window.HSStaticMethods && typeof window.HSStaticMethods.autoInit === 'function') {
        window.HSStaticMethods.autoInit();
    } else {
        console.error("HSStaticMethods.autoInit is not defined");
    }
});

// document.addEventListener('livewire:navigated', () => { 
//     window.HSStaticMethods.autoInit();
// })