<?php

namespace App\Livewire;

use App\Helpers\CartManagement;
use App\Livewire\Partials\Navbar;
use App\Models\Shop\Category;
use Livewire\Attributes\Title;
use Livewire\Component;
use  App\Models\Shop\Brand;
use App\Models\Shop\Product;
use Jantinnerezo\LivewireAlert\LivewireAlert;
use Livewire\Attributes\Url;
use Livewire\WithPagination;

class HomePage extends Component
{
    use LivewireAlert;
    use WithPagination ;
#[Title('Home Page')]
#[Url]
public $selected_categories = [];

#[Url]
public $selected_brands = [];

#[Url]
public $price_range = 5000 ;

#[Url]
public $sort = "latest" ;

    // add product to cart method
    public function addToCart($product_id) {
        $total_count=CartManagement::addItemToCart($product_id);
        $this->dispatch('update-cart-count',total_count:$total_count)->to(Navbar::class);
        $this->alert('success', 'Product added to cart successfully!', [
            'position' => 'bottom-end',
            'timer' => 3000,
            'toast' => true,
           ]);
    }
    public function render()
    {
        $productquery = Product::query()->where('is_visible',1);

        if (!empty($this->selected_categories)) {
            $productquery->whereHas('categories', function ($query) {
                $query->whereIn('shop_categories.id', $this->selected_categories);
            });
        }
        if(!empty($this->selected_brands)){
            $productquery->whereIn('shop_brand_id',$this->selected_brands);
        }        
        if($this->price_range> 0){
            $productquery->whereBetween('price',[0,$this->price_range]);
        } else {
            $productquery->where('price', '<', 0);
        }
        if($this->sort == "latest"){
            $productquery->latest();
        } 
        if($this->sort == "price"){
            $productquery->orderBy('price');
        }   

        $brands= Brand::where('is_visible',1)->get();
        $categories= Category::where('is_visible',1)->get();
        // dd($brands);
        return view('livewire.home-page',[
            "brands"=>$brands,
            "categories"=>$categories ,
            'products'=>$productquery->paginate(6),
            // 'brands' => Brand::where('is_visible',1)->get(['id','name','slug']),
            // 'categories' => Category::where('is_visible',1)->get(['id','name','slug'])
        ]);
    }
}

