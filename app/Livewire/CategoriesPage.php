<?php

namespace App\Livewire;

use App\Models\Shop\Category;
use Livewire\Attributes\Title;
use Livewire\Component;

class CategoriesPage extends Component
{
#[Title('Categories Page')]
    public function render()
    {
        $categories= Category::where('is_visible',1)->get();
        return view('livewire.categories-page',[
            'categories'=>$categories
        ]);
    }
}
