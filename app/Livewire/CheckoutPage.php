<?php

namespace App\Livewire;

use App\Helpers\CartManagement;
use App\Models\Address;
use App\Models\Shop\Order;
use Livewire\Attributes\Title;
use Livewire\Component;
#[Title('Checkout')] 
class CheckoutPage extends Component
{
    
    public $first_name; 
    public $last_name; 
    public $phone;
    public $street_address;
    public $city;
    public $state;
    public $zip_code;
    public $payment_method;

public function PlaceOrder(){
    $cart_items = CartManagement::getCartItemsFromCookie();
    $line_items = [];
    
foreach ($cart_items as $item) {
    $line_items[] = [
    'price_data' => [
    'currency' => 'USD',
    'unit_amount' => $item['unit_amount'] * 100,
    'product_data' => [
            'name' => $item['name'],
        ],
    ],
        'quantity' => $item['quantity'],
    ];
    }
    $order = new Order();
    $order->shop_customer_id = auth()->user()->id;
    $order->total_price = CartManagement::calculateGrandTotal($cart_items);
        
    // $order->payment_method = "cash";
    // $order->payment_status = 'pending';
    $order->status = 'new' ;
    $order->currency = 'USD';
    $order->shipping_price = 0;
    $order->shipping_method = 'none';
    $order->notes = 'Order placed by'. auth()->user()->name;

    $address = new Address();
    // $address->first_name = $this->first_name;
    // $address->last_name = $this->last_name;
    // $address->phone = $this->phone;
    $address->street = $this->street_address;
    $address->city = $this->city;
    $address->state = $this->state;
    $address->zip = $this->zip_code;
    // $redirect_url = '';
    // if($this->payment_method == 'stripe'){
    // } else {

    // }
    $order->save();
    // $address->order_id =$order->id;
    $address->save();
    $order->items()->createMany(($cart_items));
    CartManagement::clearCartItems();
    return redirect("/");
}

    public function render()
    {
        $cart_items = CartManagement::getCartItemsFromCookie();
        $grand_total = CartManagement::calculateGrandTotal($cart_items);
        return view('livewire.checkout-page',[
            'cart_items'=>$cart_items,
            'grand_total'=>$grand_total,
        ]);
    }
}
