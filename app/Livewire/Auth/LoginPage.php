<?php

namespace App\Livewire\Auth;

use App\Models\User;
use Livewire\Attributes\Title;
use Livewire\Component;
#[Title('login')]
class LoginPage extends Component
{
    public $email;
    public $password;
    public function save() {
        $this->validate([
            'email' => 'required|email|exists:users,email',
            'password' => 'required|min:6|max:255',
        ]);

        if (!auth()->attempt(['email' => $this->email, 'password' => $this->password])) { 
            session()->flash('error', 'Invalid credentials');
            return;
        }
        return redirect()->intended();
        
    }
    public function render()
    {
        return view('livewire.auth.login-page');
    }
}
